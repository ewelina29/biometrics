This is a simple desktop Java application providing some image processing features, like:

-scaling,

-checking and changing RGB of every chosen pixel.

-putting on some filters and masks

-creating histograms 

-thresholding

-morphology transformations

-and much more

If you wanna check it out, just clone the repository or download it and run. 
