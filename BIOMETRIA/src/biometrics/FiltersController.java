package biometrics;

import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.io.IOException;


/**
 * Created by boska - 06-04-17
 */
public class FiltersController {

    @FXML
    private Button okButton;

    private int[][] mask;
    private ObservableList<Node> children;
    private Controller controller;


    protected Stage window;

    public void display(Controller controller, int[][] mask) throws IOException {
        this.controller = controller;
        this.mask = mask;


        window = new Stage();
        window.initModality(Modality.APPLICATION_MODAL);
        Parent root = FXMLLoader.load(getClass().getResource("filtersMode.fxml"));
        window.setScene(new Scene(root, 300, 300));
        children = ((GridPane) root.getChildrenUnmodifiable().get(0)).getChildren();
        window.showAndWait();

    }


    public int[][] getMask() {
        mask = new int[3][3];


        for (Node node : children) {
            System.out.println("elo");
            int i = GridPane.getColumnIndex(node);

            int j = GridPane.getRowIndex(node);


            TextField t1 = (TextField) node;

            mask[j][i] = Integer.valueOf(t1.getText());
            System.out.println("mask: " + i + " " + j + " = " + mask[j][i]);
        }

        return mask;
    }

    public void handleOkButton() {
        okButton.getScene().getWindow().hide();
    }


}